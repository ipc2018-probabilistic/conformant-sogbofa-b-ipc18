Bootstrap: docker
From: ubuntu:bionic

%setup
     ## The "%setup"-part of this script is called to bootstrap an empty
     ## container. It copies the source files from the branch of your
     ## repository where this file is located into the container to the
     ## directory "/planner". Do not change this part unless you know
     ## what you are doing and you are certain that you have to do so.

    REPO_ROOT=`dirname $SINGULARITY_BUILDDEF`
    cp -r $REPO_ROOT/ $SINGULARITY_ROOTFS/planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install default-jre default-jdk dos2unix

    ## Build your planner
    cd /planner/planner
    rm -rf bin
    find . -type f -print0 | xargs -0 dos2unix
    bash compile

%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    INSTANCE=$1
    PORT=$2

    ## Call your planner
    cd /planner/planner
    bash run rddl.competition.Team3 $INSTANCE $PORT

## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name                  Conf-SOGBOFA
Description           Same as SOGBOFA, but adding conformant actions in the trajectory. Conformant depth is set to be the same as search depth
Authors               Hao Cui, Roni Khardon <hao.cui@tufts.edu>
SupportsIntermFluents yes
SupportsEnums         no